//What module will we need to create our Nodejs server? http
//Import http module and save it in our http variable to have access to its methods
//Ex. createServer() method.
let http = require("http");

//Mock Data for Users
let users = [
	
	{
		name: "Kim Dahyun",
		email: "kdahyun@gmail.com",
		password: "kdtwice9"
	},
	{
		name: "Lalisa Manoban",
		email: "lisabp@gmail.com",
		password: "blackpink4"
	}

];

//Mock Data for Courses
let courses = [
	
	{
		name: "PHP-Laravel 101",
		description: "Learn the basics of PHP and its framework, Laravel.",
		price: 25000
	},
	{
		name: "Python-Django 101",
		description: "Learn the basics of Python-Django.",
		price: 25000
	}

]

//Modules are pieces of software which can help and add into application.
//JS considers modules as objects.
//to access createServer() we use dot notation on our http module.

//createServer() has an anonymous function as its argument.
//This function will handle our request and our response.
//requests come from the client (web pages,browsers, other applications)
//responses come from the server.
http.createServer((req,res)=>{

	/*
		HTTP Methods allow us to group and organize our routes.

		This will allow us to create routes with the same endpoint but different routes.

		We can now create routes that may handle the same documents but does different actions.

		GET method - GET method request indicates that we want to retrieve or get data from our server to client. (RETRIEVE)

		POST method - POST method request indicates that we want to create/add a new document into our server or in our databases.(CREATE)

		PUT method - PUT method request indicates that we want to update a document in our server. (UPDATE)

		DELETE method - DELETE method request indicates that we want to delete a document in our server. (DELETE)

	*/

	console.log(req.method);

	//routes - way to handle the different requests by differing endpoints.
	//http://localhost:4000/users - /users endpoint
	//Routes have to first check the endpoint of the request url.
	//req.url is a property of req object. It contains the endpoint of our request URL.
	if(req.url === '/' && req.method === "GET"){

		//what method of the response will allow us to add the necessary additional information or headers for our response?
		res.writeHead(200,{'Content-Type': 'text/plain'});

		//How can we end the response and send our response to the client?
		res.end("Hello from our new server!")

	}

	if(req.url === '/greeting' && req.method === "GET"){

		res.writeHead(200,{'Content-Type': 'text/plain'});
		res.end('Hello this is our new greeting!');

	}

	if(req.url === '/' && req.method === "POST"){
		res.writeHead(200,{'Content-Type': 'text/plain'});
		res.end('This route will allow us to create a new document.');
	}

	/*
		Mini-Activity:

		Create a new route which first checks for '/greeting' endpoint but with POST method.
		Add a res.writeHead for the proper http status code and Content type header.
		Add a res.end() to send our message:
			"This route checks the /greeting endpoint and is a POST method request."

		Take a screenshot of the response in postman and send in our hangouts.

	*/

	if(req.url === '/greeting' && req.method === "POST"){
		res.writeHead(200,{'Content-Type': 'text/plain'});
		res.end('This route checks the /greeting endpoint and is a POST method request.');
	}

	/*
		Mini-Activity:

		Create a new route. 
			-'/' endpoint
			-'PUT' method.
		Add a res.writeHead() for the proper http status code and Content type header for our response.
		Add a res.end() to send our message:
			'This is a PUT method request. This route can be used to update documents in our database.'

		Create a new route. 
			-'/' endpoint
			- 'DELETE' method.
		Add a res.writeHead() for the proper http status code and Content type header for our response.
		Add a res.end() to send our message:
			'This is a DELETE method request. This route can be used to delete documents in our database.'

		Take a screenshot of the responses in postman and send in our hangouts.

	*/

	if(req.url === '/' && req.method === "PUT"){
		res.writeHead(200,{'Content-Type': 'text/plain'});
		res.end('This is a PUT method request. This route can be used to update documents in our database.');
	}

	if(req.url === '/' && req.method === "DELETE"){
		res.writeHead(200,{'Content-Type': 'text/plain'});
		res.end('This is a DELETE method request. This route can be used to delete documents in our database.');
	}

	//route to GET users array
	if(req.url === '/users' && req.method === "GET"){
		//Content-Type: application/json, allows us the client to "know" that the incoming response is in JSON format.
		res.writeHead(200,{'Content-Type': 'application/json'});
		//res.end() only allows strings to be passed, so, we will stringify our array first into proper JSON format.
		res.end(JSON.stringify(users));
	}

	//route to POST a new user in the users array with details coming from our client.
	if(req.url === '/users' && req.method === "POST"){

		//This route should allow us to receive data input from our client and add a new user object in our users array.

		//This will act as a placeholder for the body of our POST request.
		let requestBody = "";

		//For our Nodejs server to receive data or the body of our request (JSON sent from client), we have to create functions for 2 events:

		//req.on('data') - this will allow us to catch the stream of data from our client.

		req.on('data', function(data){
			//console.log(data);
			//we save the stream of data into our variable.
			requestBody += data;

		})

		//req.on('end') will be triggered once the data stream ends.
		//console.log(requestBody);
		req.on('end', function(){

			//console.log(requestBody);
			//Once, the end of data stream is done, the requestBody variable now contains the JSON sent from our client.
			//We have to turn the JSON into a proper JS object:
			//update the requestBody variable with a parsed version of itself.
			requestBody = JSON.parse(requestBody);

			//Push the parsed JSON into our users array
			users.push(requestBody);
			//console.log(users);

			//send the updated users array as response:
			res.writeHead(200,{'Content-Type': 'application/json'})
			res.end(JSON.stringify(users));

		})

	}

	/*
		Mini-Activity:

		Create a route which will be able to send the courses array in the client:
			-endpoint: '/courses'
			-method: 'GET'
			-Add res.writeHead() to send the proper status code and header to the client
			-send the courses array to the client using res.end()

		-Take a screenshot of your response and send it in the hangouts.

	*/




	//route to GET courses array
	if(req.url === '/courses' && req.method === "GET"){

		res.writeHead(200,{'Content-Type': 'application/json'});
		res.end(JSON.stringify(courses));
	}


	/*
		Mini-Activity:

		Create a route which will be able to send the delete the last item in our courses array and send the updated array in the client:
			-endpoint: '/courses'
			-method: 'DELETE'
			-Add res.writeHead() to send the proper status code and header to the client
			-send the courses array to the client using res.end()

				-Stretch Goal
					Do not use an array method to delete the last item in the array.

		-Take a screenshot of your response and send it in the hangouts.

	*/
	if(req.url === '/courses' && req.method === "DELETE"){

		courses.pop()
		res.writeHead(200,{'Content-Type': 'application/json'});
		res.end(JSON.stringify(courses));
	}


}).listen(4000);

console.log(`Server is running on localhost:4000`);



